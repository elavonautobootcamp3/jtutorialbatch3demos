package com.elavon.jtutorial;

public class EnhancedFoDemo {

	public static void main(String[] args) {
		// String[] players = {"Lebron","Harden","Curry","Kyrie"};
		//
		// // for(int ctr=0; ctr<players.length; ctr++) {
		// // System.out.println(players[ctr]);
		// // }
		//
		// for(String p : players) {
		// System.out.println(p);
		// }

		String[] fruits = { "banana", "apple", "mango", "orange" };
		for (String fr : fruits) {
			System.out.println(fr);
		}

	}

}
