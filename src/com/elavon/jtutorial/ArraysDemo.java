package com.elavon.jtutorial;

public class ArraysDemo {

	public static void main(String[] args) {
		int[] arrLiterals = { 101, 102, 103 };
		int[] emptyIntArr = {};
		
//		System.out.println(arrLiterals.length);
//		System.out.println(emptyIntArr.length);
		
		String[] strLitArr = { "one", "two", "three", "four" };

		String str = "Hello";
		String[] strArr = { "World", "How", str, "are", " ", "you" + "?" };

		Object[] objArr = { new Object(), new Object() };
		emptyIntArr = arrLiterals;
		
		System.out.println("Square has " + strLitArr[3] + strArr[4] +"sides");
		System.out.println(arrLiterals[1]);
		System.out.println(emptyIntArr[0]);
		System.out.println(strArr[2] + strArr[1] + strArr[5]);
		System.out.println(objArr[0]);



		
	}

}
