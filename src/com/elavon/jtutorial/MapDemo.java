package com.elavon.jtutorial;

import java.util.HashMap;
import java.util.Map;

public class MapDemo {

	public static void main(String[] args) {
		Map<Integer,String> map = new HashMap<>();
		
		map.put(1,"Pam");
		map.put(2,"RJ");
		map.put(3,"Ace");
		map.put(4,"Johnrey");
		map.put(5,"Carla");
		map.put(6,"Joshua");
		map.put(7,"Mafe");
		map.put(8,"Jen");
		
		map.put(4, "Mich");
		
		map.remove(5);
		
		for(int key : map.keySet()) {
			System.out.println(key + " " + map.get(key));
		}
		
		


	}

}
