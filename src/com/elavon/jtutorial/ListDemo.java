package com.elavon.jtutorial;

import java.util.ArrayList;
import java.util.List;

public class ListDemo {

	public static void main(String[] args) {
		List<String> list = new ArrayList<>();
		
		list.add("Jordan");
		list.add("Malone");
		list.add("Rodman");
		list.add("Kobe");
		
		list.remove(0);
		list.remove("Rodman");
		list.remove("Thomas");
		
		list.add(0, "Clarkson");
		
		
		list.add(list.size(), "Hayward");
		System.out.println("Contains Jordan? " + list.contains("Jordan"));
		
		list.clear();
		
		System.out.println(list.size());
		for(String s : list) {
			System.out.println(s);
		}

	}

}
