package com.elavon.jtutorial;

public class BoardDemo {

	public static void printBoard(char[][] board) {
		for (int x = 0; x < board[0].length; x++) {
			for (int y = 0; y < board[0].length; y++) {
				System.out.print(board[x][y]);
			}
			System.out.println();
		}
	}

	public static void main(String[] args) {
		char[][] board = new char[3][3];
		for (int x = 0; x < board[0].length; x++) {
			for (int y = 0; y < board[0].length; y++) {
				board[x][y] = '-';
			}
		}
		printBoard(board);
	}

}
