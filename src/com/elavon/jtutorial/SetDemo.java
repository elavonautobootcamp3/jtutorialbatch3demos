package com.elavon.jtutorial;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SetDemo {

	public static void main(String[] args) {
		List<Integer> list = new ArrayList<>();
		list.add(1);
		list.add(5);
		list.add(9);
		
		Set<Integer> set = new HashSet<>();
		
		set.add(1);
		set.add(1);
		set.add(4);
		set.add(5);
		set.add(4);
		set.add(9);
		set.add(5);
		
		set.remove(4);
		set.remove(99);
		
		System.out.println("Contains 5? " + set.contains(5));
		System.out.println("Contains " +  list + " ? " + set.containsAll(list));
		
		System.out.println("Size:" + set.size());

		for(int e : set) {
			System.out.println(e);
		}
		
	}

}
